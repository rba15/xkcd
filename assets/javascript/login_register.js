function sign_in(){
	
	var username = $("#username").val().trim();
	var password = $("#password").val().trim();
	
	var data = {
		required:"login",
		username:username,
		password:password
	}

	$.ajax({url: "./router.php", type:"POST" , data:data ,success: function(jsonResponse){

    	response = JSON.parse(jsonResponse); // parse the json response

    	if(response.result == "error")
				$(".error").html(response.error_message); // display error message to the user	

		else if(response.result == "success"){ // registration successful, go to home page

			if(response.is_admin)
				window.location.assign("./Views/admin_home.php"); // go to admin homepage
			else
				window.location.assign("./Views/home.php"); // go to user homepage
		}
	}});
		
	return false;
}

function create_account(){

	var firstName = $("#first_name").val().trim();
	var lastName = $("#last_name").val().trim();
	var username = $("#username").val().trim();
	var password = $("#password").val().trim();
	var passwordConfirm = $("#password_confirm").val().trim();
		
	var data = {

		required:"create_account",
		first_name:firstName,
		last_name:lastName,
		username:username,
		password:password,
		password_confirm:passwordConfirm
	}

	$.ajax({url: "../router.php", type:"POST" , data:data ,success: function(jsonResponse){

		response = JSON.parse(jsonResponse); // parse the json response

    	if(response.result == "error")
				$(".error").html(response.error_message); // display error message to the user		

		else if(response.result == "success") 
			window.location.assign("./home.php"); // login successful, go to home page
	}});
		
	return false;
}