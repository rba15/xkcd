
var currentImage, prefetchedImage;

function userOnload(){

	// get the user's history of images

	currentImage = undefined;
	prefetchedImage = undefined;
	data = {required:"image_viewer"};
	$.ajax({url: "../router.php", type:"POST" , data:data ,success: function(jsonResponse){

		response = JSON.parse(jsonResponse);

		if(response.result == "success"){

			viewedImages = response.viewed_images; // array of all the images previously liked/disliked by this user
			imageDisplay = response.image_display; // the view where the images will be displayed
	    	$("#content").html(imageDisplay);
	    	getFirstImage(viewedImages); // get first image to load to user
	    } else{
	    	alert("An error has occurred");
	    }
    }});
}

function adminOnload(){

	// get all users in our database

	data = {required:"get_all_users"};
	$.ajax({url: "../router.php", type:"POST" , data:data ,success: function(jsonResponse){

		response = JSON.parse(jsonResponse);

		if(response.result == "success"){

			usersDisplay = response.users_display; // the view where the users will be displayed
	    	$("#content").html(usersDisplay);
	    	loadTable("all_users");
	    } else{
	    	alert(response.error_message);
	    }
    }});
}
/*
function that connects to xkcd server to get the first image to display
*/
function getFirstImage(viewedImages){
	getMaxPossibleId(viewedImages);
}

/*
function that connects to xkcd server to get the max possible id
*/
function getMaxPossibleId(viewedImages){

	mostRecent = "https://xkcd.com/info.0.json"; // link to most recent image on xkcd
	$.ajax({url:mostRecent, success:function(response){

		maxId = response.num; // max possible id
		getNewImage(viewedImages, maxId);
	}});
}

/*
function that connects to xkcd server to get a new image
*/
function getNewImage(viewedImages, maxId){

	// generate random id not viewed by user
	nextId = -1;
	while(nextId == -1 || viewedImages.some(image => image.image_id == nextId)){
		nextId = Math.floor(Math.random() * (maxId - 1) + 1); // generate new id between 1 and maxId
	}

	viewedImages.push(nextId); // append this image to the list of viewed images

	imageLink = "https://xkcd.com/" + nextId + "/info.0.json"; // path to image

	$.ajax({url:imageLink, success:function(response){

		if(currentImage == undefined && prefetchedImage == undefined) { // first fetch
			currentImage = response;
			getNewImage(viewedImages, maxId); // fetch first prefetch
		}

		else if(prefetchedImage == undefined){ // first prefetch
			prefetchedImage = response;
			displayImage(currentImage.num, maxId, viewedImages, currentImage); // display first image
		}
		else{
			// other fetches
			currentImage = prefetchedImage;
			prefetchedImage = response;
		}
	}});
}

function displayImage(imageId, maxId, viewedImages, response){

	$("#current_image").attr('src',response.img); // show the generated image

	// update onclick events of like and dislike buttons 
	$("#like").unbind().click(function(){
		likeImage(imageId, response.safe_title, response.img, 1, viewedImages, maxId);
	});

	$("#dislike").unbind().click(function(){
		likeImage(imageId, response.safe_title, response.img, 0, viewedImages, maxId);
	});		
}
/*
function that inserts the like into the database
*/
function likeImage(imgId, imgTitle, imgLink, liked, viewedImages, maxId){

	data = {required: "like_image", 
			image_id:imgId, 
			image_title:imgTitle, 
			image_link:imgLink, 
			liked:liked};

	$.ajax({url:"../router.php",data:data,type:"POST",success: function(jsonResponse){
		displayImage(prefetchedImage.num, maxId, viewedImages, prefetchedImage);
		getNewImage(viewedImages, maxId);
	}});
}

function viewActivity(userId){

	data = {required:"user_activity"};

	if(userId != undefined){
		data["user_id"] = userId;
		fullName = $("#" + userId +" td:nth-child(2)").text() + " " + $("#" + userId +" td:nth-child(3)").text();
	}

	else{
		fullName = "";
	}
	
	$.ajax({url:"../router.php", data:data, type:"POST", success: function(jsonResponse){

		response = JSON.parse(jsonResponse);

		if(response.result == "success"){

			activity = response.viewed_images; // array of all the images previously liked/disliked by this user
			activityDisplay = response.image_display; // the view where the activity will be displayed
	    	$("#content").html(activityDisplay);
	    	loadTable("user_activity");

	    	if(fullName != ""){
	    		
	    		var butt = $('<input type="button" value="Back"/>');
	    		butt.click(function(){
					location.reload();
				});

	    		var p = $("<p>" + fullName + "'s Activity</p>")
        		$("#name_display").append(butt);
        		$("#name_display").append(p);
	    	}
	    	else{
	    		$("#name_display").append("<p> Your Activity</p>");
	    	}
	    } else{
	    	alert("An error has occurred");
	    }
	}});
}

function logout(){

	data = {required:"logout"};
	$.ajax({url:"../router.php", data:data, type:"POST", success: function(){
		location.reload();
	}});	
}

function loadTable(tableId){

    $('#' + tableId + ' thead tr').clone(true).appendTo( '#' + tableId + ' thead' );
    $('#' + tableId + ' thead tr:eq(1) th').each( function (i) {
        var title = $(this).text();
        if(i == 4){
	        $(this).html("");
        	var select = $('<select><option value="">Filter</option><option value="Disliked">Disliked</option><option value="Liked">Liked</option></select>')
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        table.column(i)
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
            select.appendTo(this);
        }

        else{
	        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
	 
	        $( 'input', this ).on( 'keyup change', function () {
	            if ( table.column(i).search() !== this.value ) {
	                table
	                    .column(i)
	                    .search( this.value)
	                    .draw();
	            }
	        } );
	    }
    } );
 
    var table = $('#' + tableId).DataTable( {
        orderCellsTop: true,
        fixedHeader: true
    });
}



