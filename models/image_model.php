<?php

include_once("./connection/database.php");
include_once("./classes/image.php");

class ImageModel{

	public $db;

	function __construct(){

		// establish connection to database
		$database = new Database();
		$this->db = $database->db;	
	}

	/* function that returns array of all previously liked/disliked photos by this user */

	function get_view_history($user_id){

		$select_stmt = $this->db->prepare("SELECT * FROM user_likes WHERE user_id = :user_id ORDER BY date_liked DESC");
		$select_stmt->bindParam(":user_id", $user_id);
		$select_stmt->execute();

		$history = array(); 

		if($select_stmt->rowCount() > 0){

			$result = $select_stmt->fetchAll();

			foreach ($result as $hist) {
				array_push($history, new Image($hist["image_id"], $hist["image_title"], $hist["image_link"], $hist["liked"], $hist["date_liked"]));
			}
		}
		
		return $history;
	}

	/* function that inserts new like into the database */
	
	function like_image($user_id, $image_id, $image_title, $image_link, $liked){

		try{

			$insert_statement = $this->db->prepare("INSERT INTO user_likes VALUES (:user_id, :image_id, :image_title, :image_link, :liked, NOW())");

		    $insert_statement->bindParam(':user_id', $user_id);
		    $insert_statement->bindParam(':image_id', $image_id);
		    $insert_statement->bindParam(':image_title', $image_title);
		    $insert_statement->bindParam(':image_link', $image_link);
		    $insert_statement->bindValue(':liked', $liked,  PDO::PARAM_INT);

		    $insert_statement->execute(); // execute insert statement

		    return "success";

		} catch(Exception $e) {
			return "result";
		}

	}
}