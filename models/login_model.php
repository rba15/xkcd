<?php
include_once("./connection/database.php");
include_once("./classes/user.php");

ini_set('display_errors', 'On');
ini_set('html_errors', 0);

class LoginModel{
	
	public $db;
	
	public function __construct(){
		
		// establish connection to database
		$database = new Database();
		$this->db = $database->db;
		
	}
	
	public function validate_user($first_name, $last_name, $username, $password, $confirm_password){

		$query_statement = $this->db->prepare("SELECT * FROM user WHERE username = :username");
		$query_statement->bindParam(':username', $username);
		$query_statement->execute();

		$error_message = "";

		if($query_statement->rowCount() > 0){
			$error_message .= "Username already taken<br>";
		}
		
		if(!preg_match("/^[A-Za-z0-9]+$/", $username)){
			$error_message .= "Username must contain letters and numbers only<br>";
		}
		
		if(!preg_match("/^[A-Za-z]+$/", $first_name) || !preg_match("/^[A-Za-z]+$/", $last_name)){
			$error_message .= "First name and last name must contain letters only<br>";
		}

		if($password != $confirm_password){
			$error_message .= "Passwords do not match<br>";
		}

		if(!preg_match("/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/", $password)){
			$error_message .= "Password must be made up of 8 characters<br>and contain upper and lower case letters, numbers, and a special character<br>";
		}

		return $error_message;
	}

	public function create_account($first_name, $last_name, $username, $password, $confirm_password){
		
		$error_message = $this->validate_user($first_name, $last_name, $username,$password, $confirm_password); // validate user input

		if($error_message != ""){ // invalid registration details
			return array("result" => "error", "error_message" => $error_message);
		}

		try{

			$salt = bin2hex(random_bytes(10)); // create salt
			$password = md5($password.$salt); // secure password

			/* insert new user into the database */

			$insert_statement = $this->db->prepare("INSERT INTO user VALUES (NULL,:username,:first_name,:last_name,:password,:salt,0)");

		    $insert_statement->bindParam(':username', $username);
		    $insert_statement->bindParam(':first_name', $first_name);
		    $insert_statement->bindParam(':last_name', $last_name);
		    $insert_statement->bindParam(':password', $password);
		    $insert_statement->bindParam(':salt', $salt);

		    $insert_statement->execute(); // execute insert statement

		    // get the id of the user just inserted
		    $user_id = $this->db->prepare("SELECT user_id FROM user WHERE username = :username");
		    $user_id->bindParam(":username",$username);
		    $user_id->execute();
			$user_id = $user_id->fetch();
		   	$user_id = $user_id["user_id"];

		    return array("result" => "success", "user" => new User($user_id, $username,$first_name,$last_name,0));

		} catch(Exception $e) {
			return array("result" => "error", "error_message" => "Error creating account");
		}
	}

	public function login($username, $pass){
		
		$query_statement = $this->db->prepare("SELECT * FROM user WHERE username = :username");
		$query_statement->bindParam(':username', $username);
		$query_statement->execute();

		if($query_statement->rowCount() <= 0){
			return array("result" => "error", "error_message" => "Invalid username or password");
		}
		
		else {
			
			$result = $query_statement->fetch();
			$salt = $result["salt"];
			$password = $result["password"];
			$pass = $pass.$salt;
			
			if(md5($pass) == $password){

				return array("result" => "success", "user" => new User($result["user_id"], $result["username"], $result["first_name"],$result["last_name"],$result["admin"]));
			}
			
			else
				return array("result" => "error", "error_message" => "Invalid username or password");
			
		}
	}

		/* function that returns array of all previously liked/disliked photos by this user */

	function get_all_users(){

		$select_stmt = $this->db->prepare("SELECT * FROM user WHERE admin = 0");
		$select_stmt->execute();

		$users = array(); 

		if($select_stmt->rowCount() > 0){

			$result = $select_stmt->fetchAll();

			foreach ($result as $user) {
				array_push($users, new User($user["user_id"], $user["username"], $user["first_name"],$user["last_name"],$user["admin"]));
			}
		}
		
		return $users;
	}
}