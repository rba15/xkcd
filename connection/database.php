<?php

class Database{
	
	public $db;
	
	function __construct(){
		try {
    		$this->db = new PDO("mysql:host=localhost;dbname=xkcd", "root", "root");

		} catch (PDOException $e) {
    		echo 'Connection failed: ' . $e->getMessage();
		}	
	}
}