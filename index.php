<?php
ini_set('display_errors', 'On');
ini_set('html_errors', 0);

include_once("./classes/user.php");
session_start();
if(isset($_SESSION["user"])){
	$user = $_SESSION["user"];
	echo $user->admin;
	if($user->admin)
		header("Location: ./Views/admin_home.php");
	else
		header("Location: ./Views/home.php");
}
?>

<!DOCTYPE html5>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" href="assets/stylesheets/login_register.css" />
		<script src="./assets/javascript/jquery.min.js"></script>
		<script src="./assets/javascript/login_register.js"></script>
		<script type="text/javascript"></script>
		<title>Login</title>
	</head>
	<body id = "login_body">
		<form name = "login" method="post" onsubmit = "return sign_in()">
			<h3>LOGIN</h3>
			<input type = "text" placeholder="username" id = "username" name = "username" required="required"/><br />
			<input type="password" placeholder="password" id = "password" name = "password" required="required"/><br />
			<input type="submit" id = "login" class = "login" value="Login"/>
			<a href="./Views/create_account.php">Create an Account</a>
		</form>
		<p class="error"></p>
	</body>
</html>