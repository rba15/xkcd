<?php

ini_set('display_errors', 'On');
ini_set('html_errors', 0);

include_once("./classes/user.php");	
include_once("./controllers/login_controller.php");
include_once("./controllers/image_controller.php");

$image_controller = new ImageController(); // create new image controller
$login_controller = new LoginController(); // create new login controller

if(isset($_POST["required"])){

	$required = $_POST["required"];

	if($required == "create_account"){
	
		if(isset($_POST["username"]) && isset($_POST["password"]) && isset($_POST["password_confirm"]) && isset($_POST["first_name"]) && isset($_POST["last_name"])){
			
			// registration
			$username = $_POST["username"];
			$password = $_POST["password"];
			$password_confirm = $_POST["password_confirm"];
			$first_name = $_POST["first_name"];
			$last_name = $_POST["last_name"];

			echo $login_controller->create_account($first_name, $last_name, $username, $password, $password_confirm);
		}

		else{
			echo json_encode(array("result" => "error", "error_message" => "Missing fields"));
		}
	}

	elseif($required == "login"){
		
		if(isset($_POST["username"]) && isset($_POST["password"])){
			
			$username = $_POST["username"];
			$password = $_POST["password"];
			echo $login_controller->login($username,$password);
		}

		else{
			echo json_encode(array("result" => "error", "error_message" => "Missing fields"));
		}
	}

	elseif($required == "logout"){
		
		session_start();
		if(isset($_SESSION["user"])){
			$login_controller->logout();
		}
		echo json_encode(array("result" => "success"));
	}

	elseif($required == "image_viewer" || $required == "user_activity"){

		session_start();

		if(isset($_SESSION["user"])){

			$user = $_SESSION["user"];
			if(isset($_POST["user_id"]))
				$user_id = $_POST["user_id"];
			else
				$user_id = $user->user_id;
			echo $image_controller->get_view_history($user_id, $required); // return array of all images previously liked and disliked by the user
		}
		else{
			echo json_encode(array("result" => "error", "error_message" => "Missing fields"));
		}
	}

	elseif($required == "get_all_users"){

		session_start();

		if(isset($_SESSION["user"])){

			$user = $_SESSION["user"];
			if($user->admin) // verify the user requesting all the users is an admin
				echo $login_controller->get_all_users(); // get all users
			else
				echo json_encode(array("result" => "error", "error_message" => "Access denied"));
		}
		else{
			echo json_encode(array("result" => "error", "error_message" => "Missing fields"));
		}		
	}

	elseif($required == "like_image"){

		session_start();

		if(isset($_POST["image_id"]) && isset($_POST["image_title"]) && isset($_POST["image_link"]) && isset($_POST["liked"]) && isset($_SESSION["user"])){
			$user = $_SESSION["user"];
			echo $image_controller->like_image($user->user_id, 
			$_POST["image_id"], $_POST["image_title"], $_POST["image_link"], $_POST["liked"]); // set the image to liked/disliked by this user
		}
		else{
			echo json_encode(array("result" => "error", "error_message" => "Missing fields"));
		}
	}

	elseif($required == "viewed_images"){

		session_start();

		if(isset($_SESSION["user"])){
			$user = $_SESSION["user"];
			echo $image_controller->get_view_history($user->user_id); // return array of all images previously liked and disliked by the user
		}
		else{
			echo json_encode(array("result" => "error", "error_message" => "Missing fields"));
		}
	}

	else{
		echo json_encode(array("result" => "error", "error_message" => "Missing fields"));
	}
}

else{
	echo "Access denied";
}