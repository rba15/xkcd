<?php

class User {
	
	public $user_id;
	public $username;
	public $first_name;
	public $last_name;
	public $admin; // boolean user admin or not
	
	public function __construct($user_id, $username,$first_name,$last_name, $admin){
		
		$this->user_id = $user_id;
		$this->username = $username;
		$this->first_name = $first_name;
		$this->last_name = $last_name;
		$this->admin = $admin;
	}	
}