<?php

class Image {
	
	public $image_id;
	public $liked; // boolean variable liked or disliked 

	public function __construct($image_id, $image_title, $image_link, $liked, $date_liked){
		
		$this->image_id = $image_id;
		$this->image_title = $image_title;
		$this->image_link = $image_link;
		$this->liked = $liked;
		$this->date_liked = $date_liked;
	}	
}