<?php
ini_set('display_errors', 'On');
ini_set('html_errors', 0);

	session_start();
	if(isset($_SESSION["user"]))
		header("Location: ./home.php");
?>

<!DOCTYPE html5>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" href="../assets/stylesheets/login_register.css" />
		<script src="../assets/javascript/jquery.min.js"></script>
		<script src="../assets/javascript/login_register.js"></script>
		<title>Create Account</title>
		<script type='text/javascript'>
	         $(document).ready(function() {
	            //option A
	            $("form").submit(function(e){
	                e.preventDefault(e);
	                return create_account();
	            });
	        });
        </script>
	</head>
	<body>
		<form name = "create_account" method="post">
			<h3>Create Account</h3>
			<input type = "text" placeholder="First Name" id = "first_name" name = "first_name" required="required"/>
			<input type = "text" placeholder="Last Name" id = "last_name" name = "last_name" required="required"/>
			<input type = "text" placeholder="Username" id = "username" name = "username" required="required"/>
			<input type="password" placeholder="Password" id = "password" name = "password" required="required"/>
			<input type="password" placeholder="Confirm Password" id = "password_confirm" name = "confirm_password" required="required"/>
			<input type="submit" id = "register_button" class = "login" value="Create Account"/>
		</form>
		<p class="error"></p>
	</body>
</html>