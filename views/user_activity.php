
<div id = "name_display"></div>
<table id = "user_activity" class=" custom-tables table-striped hover display table dataTable table-bordered table-striped">		
	<thead><th>Image ID</th><th>Image Title</th><th>Date Liked</th><th>Image Link</th><th>Liked/Dislike</th></thead>
	<?php		
		foreach($viewed_images as $image){

			if($image->liked)
				$liked = "Liked";
			else
				$liked = "Disliked";
			
			echo "<tr>".
				"<td>$image->image_id</td>".
				"<td>$image->image_title</td>".
				"<td>$image->date_liked</td>".
				"<td><a href='$image->image_link' target='_blank'>$image->image_link</a></td>".
				"<td>$liked</td>".
				"</tr>";
		}
	?>
</table>