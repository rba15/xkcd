<?php
ini_set('display_errors', 'On');
ini_set('html_errors', 0);

	include_once("../classes/user.php");

	session_start();
	
	if(!isset($_SESSION["user"])){
		header("Location: ../index.php"); // redirect to login page
	}

	else{

		$user = $_SESSION["user"];
		if(!$user->admin)
			header("Location: ./home.php");
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Admin Home</title>
		<link rel="stylesheet" type="text/css" href="../assets/stylesheets/jquery.dataTables.min.css">
		<link rel="stylesheet" type="text/css" href="../assets/stylesheets/home.css">


		<script type="text/javascript" src = "../assets/javascript/jquery.min.js"></script>
		<script type="text/javascript" src = "../assets/javascript/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src = "../assets/javascript/home.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				adminOnload();
			});
		</script>
	</head>
<body>
	<header>
	<img id ="logo" src ="../assets/images/logo.png" style="height: 5em" />
		<ul class = "navigation">
			<li id = "welcome_message">Welcome, <?php echo $user->first_name;?>!</li>
		  <li onclick="adminOnload()">Home</li>
		  <li onclick="logout()">Logout</li>
		</ul>
	</header>
	<main id = "content"><table id = "all_users"></table></main>
</body>
</html>