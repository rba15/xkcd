-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Dec 27, 2018 at 01:10 PM
-- Server version: 5.7.23
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `xkcd`
--

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `salt` varchar(50) NOT NULL,
  `admin` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `username`, `first_name`, `last_name`, `password`, `salt`, `admin`) VALUES
(14, 'roaa', 'Roaa', 'Feel', 'abeb2ba0183afb4010201bbf36a7b3e2', 'dee13a95ee8db1048997', 0),
(15, 'admin', 'Admin', 'User', '07d32da07eb39b6489aaf18468fec598', '9ea4f6c05ed17aaef570', 1),
(16, 'mohamad', 'Mohamad', 'Feel', '322bad0983670aca96254c1d9371a881', '96735c4a418ef69d0785', 0),
(17, 'bachir', 'Bachir', 'Masry', '675dc31d3bf4d66fa1535f56e08d63ab', 'e0243072f0024ed0db5e', 0),
(18, 'ali2', 'Ali', 'Sabeh', '0990d885ff628ac3b9709b640663a23a', 'f22a4667bcb5eb3d9472', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_likes`
--

CREATE TABLE `user_likes` (
  `user_id` int(11) NOT NULL,
  `image_id` int(11) NOT NULL,
  `image_title` varchar(50) NOT NULL,
  `image_link` varchar(50) NOT NULL,
  `liked` tinyint(1) NOT NULL,
  `date_liked` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_likes`
--

INSERT INTO `user_likes` (`user_id`, `image_id`, `image_title`, `image_link`, `liked`, `date_liked`) VALUES
(14, 13, 'Canyon', 'https://imgs.xkcd.com/comics/canyon_small.jpg', 1, '2018-12-27'),
(14, 14, 'Copyright', 'https://imgs.xkcd.com/comics/copyright.jpg', 0, '2018-12-27'),
(14, 57, 'Wait For Me', 'https://imgs.xkcd.com/comics/wait_for_me.jpg', 0, '2018-12-27'),
(14, 70, 'Guitar Hero', 'https://imgs.xkcd.com/comics/guitar_hero.jpg', 1, '2018-12-26'),
(14, 93, 'Jeremy Irons', 'https://imgs.xkcd.com/comics/jeremy_irons.jpg', 0, '2018-12-27'),
(14, 112, 'Baring My Heart', 'https://imgs.xkcd.com/comics/baring_my_heart.png', 1, '2018-12-25'),
(14, 117, 'Pong', 'https://imgs.xkcd.com/comics/pong.png', 0, '2018-12-26'),
(14, 120, 'Dating Service', 'https://imgs.xkcd.com/comics/dating_service.png', 1, '2018-12-27'),
(14, 133, 'The Raven', 'https://imgs.xkcd.com/comics/the_raven.jpg', 1, '2018-12-27'),
(14, 140, 'Delicious', 'https://imgs.xkcd.com/comics/delicious.png', 0, '2018-12-27'),
(14, 188, 'Reload', 'https://imgs.xkcd.com/comics/reload.png', 1, '2018-12-27'),
(14, 204, 'America', 'https://imgs.xkcd.com/comics/america.png', 1, '2018-12-27'),
(14, 239, 'Blagofaire', 'https://imgs.xkcd.com/comics/blagofaire.png', 1, '2018-12-27'),
(14, 243, 'Appropriate Term', 'https://imgs.xkcd.com/comics/appropriate_term.png', 1, '2018-12-26'),
(14, 246, 'Labyrinth Puzzle', 'https://imgs.xkcd.com/comics/labyrinth_puzzle.png', 0, '2018-12-26'),
(14, 249, 'Chess Photo', 'https://imgs.xkcd.com/comics/chess_photo.png', 0, '2018-12-26'),
(14, 255, 'Subjectivity', 'https://imgs.xkcd.com/comics/subjectivity.png', 0, '2018-12-27'),
(14, 295, 'DNE', 'https://imgs.xkcd.com/comics/dne.png', 1, '2018-12-27'),
(14, 313, 'Insomnia', 'https://imgs.xkcd.com/comics/insomnia.png', 0, '2018-12-26'),
(14, 344, '1337: Part 4', 'https://imgs.xkcd.com/comics/1337_part_4.png', 0, '2018-12-27'),
(14, 347, 'Brick Archway', 'https://imgs.xkcd.com/comics/brick_archway.png', 0, '2018-12-27'),
(14, 366, 'Your Mom', 'https://imgs.xkcd.com/comics/your_mom.png', 1, '2018-12-27'),
(14, 370, 'Redwall', 'https://imgs.xkcd.com/comics/redwall.png', 0, '2018-12-27'),
(14, 410, 'Math Paper', 'https://imgs.xkcd.com/comics/math_paper.png', 1, '2018-12-27'),
(14, 427, 'Bad Timing', 'https://imgs.xkcd.com/comics/bad_timing.png', 0, '2018-12-27'),
(14, 439, 'Thinking Ahead', 'https://imgs.xkcd.com/comics/thinking_ahead.png', 0, '2018-12-27'),
(14, 473, 'Still Raw', 'https://imgs.xkcd.com/comics/still_raw.png', 1, '2018-12-27'),
(14, 499, 'Scantron', 'https://imgs.xkcd.com/comics/scantron.png', 1, '2018-12-27'),
(14, 506, 'Theft of the Magi', 'https://imgs.xkcd.com/comics/theft_of_the_magi.png', 0, '2018-12-27'),
(14, 509, 'Induced Current', 'https://imgs.xkcd.com/comics/induced_current.png', 1, '2018-12-27'),
(14, 551, 'Etch-a-Sketch', 'https://imgs.xkcd.com/comics/etch-a-sketch.png', 0, '2018-12-27'),
(14, 567, 'Urgent Mission', 'https://imgs.xkcd.com/comics/urgent_mission.png', 0, '2018-12-27'),
(14, 572, 'Together', 'https://imgs.xkcd.com/comics/together.png', 0, '2018-12-27'),
(14, 585, 'Outreach', 'https://imgs.xkcd.com/comics/outreach.png', 0, '2018-12-27'),
(14, 615, 'Avoidance', 'https://imgs.xkcd.com/comics/avoidance.png', 0, '2018-12-27'),
(14, 622, 'Haiku Proof', 'https://imgs.xkcd.com/comics/haiku_proof.png', 0, '2018-12-27'),
(14, 628, 'Psychic', 'https://imgs.xkcd.com/comics/psychic.png', 1, '2018-12-26'),
(14, 631, 'Anatomy Text', 'https://imgs.xkcd.com/comics/anatomy_text.png', 1, '2018-12-27'),
(14, 642, 'Creepy', 'https://imgs.xkcd.com/comics/creepy.png', 0, '2018-12-26'),
(14, 655, 'Climbing', 'https://imgs.xkcd.com/comics/climbing.png', 1, '2018-12-26'),
(14, 709, 'I Am', 'https://imgs.xkcd.com/comics/i_am.png', 0, '2018-12-27'),
(14, 727, 'Trade Expert', 'https://imgs.xkcd.com/comics/trade_expert.png', 0, '2018-12-27'),
(14, 731, 'Desert Island', 'https://imgs.xkcd.com/comics/desert_island.png', 1, '2018-12-26'),
(14, 742, 'Campfire', 'https://imgs.xkcd.com/comics/campfire.png', 1, '2018-12-27'),
(14, 796, 'Bad Ex', 'https://imgs.xkcd.com/comics/bad_ex.png', 1, '2018-12-27'),
(14, 810, 'Constructive', 'https://imgs.xkcd.com/comics/constructive.png', 1, '2018-12-27'),
(14, 840, 'Serious', 'https://imgs.xkcd.com/comics/serious.png', 0, '2018-12-27'),
(14, 841, 'Audiophiles', 'https://imgs.xkcd.com/comics/audiophiles.png', 0, '2018-12-27'),
(14, 855, 'Squirrelphone', 'https://imgs.xkcd.com/comics/squirrelphone.png', 1, '2018-12-27'),
(14, 857, 'Archimedes', 'https://imgs.xkcd.com/comics/archimedes.png', 1, '2018-12-27'),
(14, 858, 'Milk', 'https://imgs.xkcd.com/comics/milk.png', 1, '2018-12-27'),
(14, 887, 'Future Timeline', 'https://imgs.xkcd.com/comics/future_timeline.png', 0, '2018-12-26'),
(14, 903, 'Extended Mind', 'https://imgs.xkcd.com/comics/extended_mind.png', 0, '2018-12-27'),
(14, 927, 'Standards', 'https://imgs.xkcd.com/comics/standards.png', 1, '2018-12-27'),
(14, 942, 'Juggling', 'https://imgs.xkcd.com/comics/juggling.png', 1, '2018-12-27'),
(14, 964, 'Dorm Poster', 'https://imgs.xkcd.com/comics/dorm_poster.png', 0, '2018-12-27'),
(14, 1001, 'AAAAAA', 'https://imgs.xkcd.com/comics/aaaaaa.png', 0, '2018-12-27'),
(14, 1036, 'Reviews', 'https://imgs.xkcd.com/comics/reviews.png', 0, '2018-12-27'),
(14, 1054, 'Theâbacon', 'https://imgs.xkcd.com/comics/thebacon.png', 0, '2018-12-27'),
(14, 1057, 'Klout', 'https://imgs.xkcd.com/comics/klout.png', 1, '2018-12-27'),
(14, 1068, 'Swiftkey', 'https://imgs.xkcd.com/comics/swiftkey.png', 0, '2018-12-27'),
(14, 1080, 'Visual Field', 'https://imgs.xkcd.com/comics/visual_field.png', 1, '2018-12-27'),
(14, 1095, 'Crazy Straws', 'https://imgs.xkcd.com/comics/crazy_straws.png', 0, '2018-12-25'),
(14, 1103, 'Nine', 'https://imgs.xkcd.com/comics/nine.png', 0, '2018-12-27'),
(14, 1104, 'Feathers', 'https://imgs.xkcd.com/comics/feathers.png', 0, '2018-12-27'),
(14, 1120, 'Blurring the Line', 'https://imgs.xkcd.com/comics/blurring_the_line.png', 0, '2018-12-27'),
(14, 1126, 'Epsilon and Zeta', 'https://imgs.xkcd.com/comics/epsilon_and_zeta.png', 1, '2018-12-27'),
(14, 1128, 'Fifty Shades', 'https://imgs.xkcd.com/comics/fifty_shades.png', 0, '2018-12-27'),
(14, 1129, 'Cell Number', 'https://imgs.xkcd.com/comics/cell_number.png', 0, '2018-12-26'),
(14, 1160, 'Drop Those Pounds', 'https://imgs.xkcd.com/comics/drop_those_pounds.png', 0, '2018-12-27'),
(14, 1178, 'Pickup Artists', 'https://imgs.xkcd.com/comics/pickup_artists.png', 1, '2018-12-27'),
(14, 1191, 'The Past', 'https://imgs.xkcd.com/comics/the_past.png', 0, '2018-12-27'),
(14, 1202, 'Girls and Boys', 'https://imgs.xkcd.com/comics/girls_and_boys.png', 1, '2018-12-27'),
(14, 1203, 'Time Machines', 'https://imgs.xkcd.com/comics/time_machines.png', 0, '2018-12-27'),
(14, 1237, 'QR Code', 'https://imgs.xkcd.com/comics/qr_code.png', 1, '2018-12-27'),
(14, 1256, 'Batman', 'https://imgs.xkcd.com/comics/batman.png', 0, '2018-12-27'),
(14, 1265, 'Juicer', 'https://imgs.xkcd.com/comics/juicer.png', 1, '2018-12-25'),
(14, 1312, 'Haskell', 'https://imgs.xkcd.com/comics/haskell.png', 0, '2018-12-27'),
(14, 1328, 'Update', 'https://imgs.xkcd.com/comics/update.png', 1, '2018-12-27'),
(14, 1376, 'Jump', 'https://imgs.xkcd.com/comics/jump.png', 0, '2018-12-27'),
(14, 1397, 'Luke', 'https://imgs.xkcd.com/comics/luke.png', 0, '2018-12-27'),
(14, 1405, 'Meteor', 'https://imgs.xkcd.com/comics/meteor.png', 1, '2018-12-27'),
(14, 1415, 'Ballooning', 'https://imgs.xkcd.com/comics/ballooning.png', 1, '2018-12-27'),
(14, 1416, 'Pixels', 'https://imgs.xkcd.com/comics/pixels.png', 1, '2018-12-27'),
(14, 1422, 'My Phone is Dying', 'https://imgs.xkcd.com/comics/my_phone_is_dying.png', 0, '2018-12-27'),
(14, 1424, 'En Garde', 'https://imgs.xkcd.com/comics/en_garde.png', 0, '2018-12-26'),
(14, 1459, 'Documents', 'https://imgs.xkcd.com/comics/documents.png', 0, '2018-12-27'),
(14, 1486, 'Vacuum', 'https://imgs.xkcd.com/comics/vacuum.png', 0, '2018-12-27'),
(14, 1495, 'Hard Reboot', 'https://imgs.xkcd.com/comics/hard_reboot.png', 0, '2018-12-25'),
(14, 1496, 'Art Project', 'https://imgs.xkcd.com/comics/art_project.png', 0, '2018-12-27'),
(14, 1503, 'Squirrel Plan', 'https://imgs.xkcd.com/comics/squirrel_plan.png', 0, '2018-12-27'),
(14, 1506, 'xkcloud', 'https://imgs.xkcd.com/comics/xkcloud.png', 0, '2018-12-27'),
(14, 1536, 'The Martian', 'https://imgs.xkcd.com/comics/the_martian.png', 0, '2018-12-26'),
(14, 1549, 'xkcd Phone 3', 'https://imgs.xkcd.com/comics/xkcd_phone_3.png', 0, '2018-12-27'),
(14, 1563, 'Synonym Movies', 'https://imgs.xkcd.com/comics/synonym_movies.png', 0, '2018-12-27'),
(14, 1581, 'Birthday', 'https://imgs.xkcd.com/comics/birthday.png', 0, '2018-12-27'),
(14, 1614, 'Kites', 'https://imgs.xkcd.com/comics/kites.png', 0, '2018-12-27'),
(14, 1631, 'Thumb War', 'https://imgs.xkcd.com/comics/thumb_war.png', 0, '2018-12-27'),
(14, 1636, 'XKCD Stack', 'https://imgs.xkcd.com/comics/xkcd_stack.png', 0, '2018-12-27'),
(14, 1639, 'To Taste', 'https://imgs.xkcd.com/comics/to_taste.png', 1, '2018-12-26'),
(14, 1653, 'United States Map', 'https://imgs.xkcd.com/comics/united_states_map.png', 0, '2018-12-27'),
(14, 1665, 'City Talk Pages', 'https://imgs.xkcd.com/comics/city_talk_pages.png', 1, '2018-12-27'),
(14, 1667, 'Algorithms', 'https://imgs.xkcd.com/comics/algorithms.png', 1, '2018-12-27'),
(14, 1678, 'Recent Searches', 'https://imgs.xkcd.com/comics/recent_searches.png', 1, '2018-12-27'),
(14, 1717, 'Pyramid Honey', 'https://imgs.xkcd.com/comics/pyramid_honey.png', 0, '2018-12-27'),
(14, 1725, 'Linear Regression', 'https://imgs.xkcd.com/comics/linear_regression.png', 0, '2018-12-27'),
(14, 1737, 'Datacenter Scale', 'https://imgs.xkcd.com/comics/datacenter_scale.png', 1, '2018-12-25'),
(14, 1746, 'Making Friends', 'https://imgs.xkcd.com/comics/making_friends.png', 0, '2018-12-27'),
(14, 1777, 'Dear Diary', 'https://imgs.xkcd.com/comics/dear_diary.png', 1, '2018-12-27'),
(14, 1798, 'Box Plot', 'https://imgs.xkcd.com/comics/box_plot.png', 1, '2018-12-26'),
(14, 1807, 'Questions', 'https://imgs.xkcd.com/comics/questions.png', 0, '2018-12-27'),
(14, 1809, 'xkcd Phone 5', 'https://imgs.xkcd.com/comics/xkcd_phone_5.png', 1, '2018-12-26'),
(14, 1827, 'Survivorship Bias', 'https://imgs.xkcd.com/comics/survivorship_bias.png', 0, '2018-12-27'),
(14, 1838, 'Machine Learning', 'https://imgs.xkcd.com/comics/machine_learning.png', 0, '2018-12-26'),
(14, 1866, 'Russell\'s Teapot', 'https://imgs.xkcd.com/comics/russells_teapot.png', 0, '2018-12-27'),
(14, 1920, 'Emoji Sports', 'https://imgs.xkcd.com/comics/emoji_sports.png', 1, '2018-12-27'),
(14, 1967, 'Violin Plots', 'https://imgs.xkcd.com/comics/violin_plots.png', 1, '2018-12-27'),
(14, 1971, 'Personal Data', 'https://imgs.xkcd.com/comics/personal_data.png', 1, '2018-12-27'),
(14, 1972, 'Autogyros', 'https://imgs.xkcd.com/comics/autogyros.png', 0, '2018-12-27'),
(14, 1999, 'Selection Effect', 'https://imgs.xkcd.com/comics/selection_effect.png', 1, '2018-12-27'),
(14, 2017, 'Stargazing 2', 'https://imgs.xkcd.com/comics/stargazing_2.png', 0, '2018-12-27'),
(14, 2023, 'Y-Axis', 'https://imgs.xkcd.com/comics/y_axis.png', 1, '2018-12-26'),
(14, 2066, 'Ballot Selfies', 'https://imgs.xkcd.com/comics/ballot_selfies.png', 0, '2018-12-27'),
(14, 2073, 'Kilogram', 'https://imgs.xkcd.com/comics/kilogram.png', 1, '2018-12-27'),
(16, 245, 'Floor Tiles', 'https://imgs.xkcd.com/comics/floor_tiles.png', 0, '2018-12-26'),
(16, 302, 'Names', 'https://imgs.xkcd.com/comics/names.png', 0, '2018-12-26'),
(16, 425, 'Fortune Cookies', 'https://imgs.xkcd.com/comics/fortune_cookies.png', 0, '2018-12-26'),
(16, 510, 'Egg Drop Failure', 'https://imgs.xkcd.com/comics/egg_drop_failure.png', 0, '2018-12-26'),
(16, 887, 'Future Timeline', 'https://imgs.xkcd.com/comics/future_timeline.png', 0, '2018-12-26'),
(16, 906, 'Advertising Discovery', 'https://imgs.xkcd.com/comics/citations.png', 1, '2018-12-26'),
(16, 936, 'Password Strength', 'https://imgs.xkcd.com/comics/password_strength.png', 0, '2018-12-26'),
(16, 1885, 'Ensemble Model', 'https://imgs.xkcd.com/comics/ensemble_model.png', 0, '2018-12-26'),
(16, 1911, 'Defensive Profile', 'https://imgs.xkcd.com/comics/defensive_profile.png', 0, '2018-12-26'),
(17, 206, 'Reno Rhymes', 'https://imgs.xkcd.com/comics/reno_rhymes.png', 0, '2018-12-27'),
(17, 226, 'Swingset', 'https://imgs.xkcd.com/comics/swingset.png', 0, '2018-12-27'),
(17, 239, 'Blagofaire', 'https://imgs.xkcd.com/comics/blagofaire.png', 0, '2018-12-27'),
(17, 301, 'Limerick', 'https://imgs.xkcd.com/comics/limerick.png', 0, '2018-12-27'),
(17, 338, 'Future', 'https://imgs.xkcd.com/comics/future.png', 1, '2018-12-27'),
(17, 340, 'Fight', 'https://imgs.xkcd.com/comics/fight.png', 1, '2018-12-26'),
(17, 388, 'Fuck Grapefruit', 'https://imgs.xkcd.com/comics/fuck_grapefruit.png', 0, '2018-12-27'),
(17, 910, 'Permanence', 'https://imgs.xkcd.com/comics/permanence.png', 1, '2018-12-27'),
(17, 1067, 'Pressures', 'https://imgs.xkcd.com/comics/pressures.png', 1, '2018-12-27'),
(17, 1074, 'Moon Landing', 'https://imgs.xkcd.com/comics/moon_landing.png', 0, '2018-12-27'),
(17, 1148, 'Nothing to Offer', 'https://imgs.xkcd.com/comics/nothing_to_offer.png', 0, '2018-12-27'),
(17, 1252, 'Increased Risk', 'https://imgs.xkcd.com/comics/increased_risk.png', 0, '2018-12-27'),
(17, 1256, 'Questions', 'https://imgs.xkcd.com/comics/questions.png', 0, '2018-12-27'),
(17, 1290, 'Syllable Planning', 'https://imgs.xkcd.com/comics/syllable_planning.png', 0, '2018-12-27'),
(17, 1379, '4.5 Degrees', 'https://imgs.xkcd.com/comics/4_5_degrees.png', 1, '2018-12-27'),
(17, 1474, 'Screws', 'https://imgs.xkcd.com/comics/screws.png', 0, '2018-12-27'),
(17, 1629, 'Tools', 'https://imgs.xkcd.com/comics/tools.png', 1, '2018-12-27'),
(17, 1717, 'Pyramid Honey', 'https://imgs.xkcd.com/comics/pyramid_honey.png', 0, '2018-12-27'),
(17, 1730, 'Starshade', 'https://imgs.xkcd.com/comics/starshade.png', 1, '2018-12-27'),
(17, 1771, 'It Was I', 'https://imgs.xkcd.com/comics/it_was_i.png', 1, '2018-12-26'),
(17, 1813, 'Vomiting Emoji', 'https://imgs.xkcd.com/comics/vomiting_emoji.png', 1, '2018-12-27'),
(17, 1831, 'Here to Help', 'https://imgs.xkcd.com/comics/here_to_help.png', 1, '2018-12-27'),
(17, 1839, 'Doctor Visit', 'https://imgs.xkcd.com/comics/doctor_visit.png', 0, '2018-12-27'),
(18, 76, 'Familiar', 'https://imgs.xkcd.com/comics/familiar.jpg', 1, '2018-12-27'),
(18, 392, 'Making Rules', 'https://imgs.xkcd.com/comics/making_rules.png', 1, '2018-12-27'),
(18, 486, 'I am Not a Ninja', 'https://imgs.xkcd.com/comics/i_am_not_a_ninja.png', 1, '2018-12-27'),
(18, 864, 'Flying Cars', 'https://imgs.xkcd.com/comics/flying_cars.png', 1, '2018-12-27'),
(18, 1288, 'Substitutions', 'https://imgs.xkcd.com/comics/substitutions.png', 1, '2018-12-27');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `user_likes`
--
ALTER TABLE `user_likes`
  ADD PRIMARY KEY (`user_id`,`image_id`),
  ADD KEY `user_id` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `user_likes`
--
ALTER TABLE `user_likes`
  ADD CONSTRAINT `user_likes_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
