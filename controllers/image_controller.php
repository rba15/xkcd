<?php
include_once("./models/image_model.php");

class ImageController{
	
	public $model;
	
	public function __construct(){
		$this->model = new ImageModel();
	}
	
	public function get_view_history($user_id, $required){

		$viewed_images = $this->model->get_view_history($user_id); // get the array of all liked and disliked images
		ob_start();
		$required_view = "./views/" . $required . ".php";
		include $required_view; // view where the image will be displayed
		$image_display = ob_get_clean();
		ob_end_flush();
		return json_encode(array("result" => "success", "viewed_images" => $viewed_images, "image_display" => $image_display));
	}

	public function like_image($user_id, $image_id, $image_title, $image_link, $liked){
		$result = $this->model->like_image($user_id, $image_id, $image_title, $image_link, $liked);
		return json_encode(array("result" => $result));
	}
}