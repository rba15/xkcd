<?php
include_once("./models/login_model.php");

class LoginController{
	
	public $model;
	
	public function __construct(){
		$this->model = new LoginModel();
	}
	
	public function create_account($first_name, $last_name, $username,$password, $confirm_password){
		
		$response = $this->model->create_account($first_name, $last_name, $username,$password, $confirm_password);
		
		if($response["result"] == "error") # invalid login
			return json_encode($response); # return error message
		
		elseif($response["result"] == "success") {
			
			$user = $response["user"];
			session_start();
			$_SESSION["user"] = $user; # save user to session

			return json_encode(array("result" => "success"));
		}
	}
		
	public function login($username,$password){
		
		$response = $this->model->login($username,$password);
		
		if($response["result"] == "error") # invalid login
			return json_encode($response); # return error message
		
		elseif($response["result"] == "success") {
			
			$user = $response["user"];
			session_start();
			$_SESSION["user"] = $user;
			return json_encode(array("result" => "success", "is_admin" => $user->admin));
		}
	}

	public function get_all_users(){

		$users = $this->model->get_all_users();
		ob_start();
		include "./views/display_users.php"; // view where the users will be displayed
		$users_display = ob_get_clean();
		ob_end_flush();
		return json_encode(array("result" => "success", "users_display" => $users_display));
	}	
	public function logout(){
		session_unset();
		session_destroy();
		header("Location: ./index.php");
	}
}